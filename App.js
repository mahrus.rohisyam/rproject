import * as React from 'react';
import {Provider} from 'react-redux';
import {Store, Persistor} from './src/redux/store';
import Router from './src/navigation/router'
import { PersistGate } from 'redux-persist/integration/react'
const App = () => {
  return (
    <PersistGate loading={null} persistor={Persistor}>
      <Provider store={Store}>
        <Router/>
      </Provider>
    </PersistGate>
  );
};
export default App;

