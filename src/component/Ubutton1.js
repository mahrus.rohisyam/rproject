import React, { Component } from 'react'
import { Text, TouchableOpacity, View } from 'react-native'

export default class Ubutton1 extends Component {
    render() {
        return (
            <TouchableOpacity style={{width:250,height:50,backgroundColor:'#ffae00',borderRadius:20,justifyContent:'center',alignItems:'center'}}>
                <Text style={{fontSize:18,color:'#1e1e1e'}}>
                    {this.props.title}
                </Text>
            </TouchableOpacity>
        )
    }
}
