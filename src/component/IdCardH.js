import React, { Component } from 'react'
import { Text, View, StyleSheet, Image } from 'react-native'
export default class IdCardH extends Component {
    render() {
        return (
            <View style={{width:'100%',height:100,backgroundColor:'#4d4d4d',flexDirection:'row',alignItems:'center',marginTop:5,borderRadius:15}}>
                    <View style={{width:75,height:75,backgroundColor:'#ffae00',margin:10,borderRadius:15,justifyContent:'center',alignItems:'center'}}>
                        {/* <MCI name='film-strip' size={50} color='#1e1e1e'/> */}
                        <Image source={{uri:'https://via.placeholder.com/150/92c952'}} style={{width:'100%',height:'100%',resizeMode:'cover'}}/>
                    </View>
                    <View style={{width:360,height:75,padding:10}}>
                        <Text style={styles.text}>ID            : {this.props.id}</Text>
                        <Text style={styles.text}>Title        : {this.props.name}</Text>
                        
                        
                    </View>
            </View>
        )
    }
}
const styles=StyleSheet.create({
    text:{
        color:'#fff'

    }
})