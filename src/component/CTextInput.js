import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
export class CTextInput extends Component {
  render() {
    const {style} = this.props;
    return (
      <View style={styles.mainView}>
        <TextInput
          placeholderTextColor="black"
          style={styles.input}
          {...this.props}
          style={{...styles.input, ...style}}
        />
        <MaterialIcons
          name={this.props.iconName}
          size={30}
          style={{...styles.iconBack, ...style}}
          {...this.props}
        />
      </View>
    );
  }
}

export default CTextInput;
const styles = StyleSheet.create({
  input: {
    backgroundColor: 'white',
    borderRadius: 20,
    width: 250,
    height: 50,
    justifyContent: 'center',
    flexDirection: 'row',
    paddingLeft: 15,
    color: 'black',
  },
  iconBack: {
    position: 'absolute',
    top: 25,
    right: 15,
  },
  mainView: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
});
