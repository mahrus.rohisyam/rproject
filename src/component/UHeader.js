import React, {Component, PureComponent} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  BackHandler,
  StyleSheet,
} from 'react-native';
import MCI from 'react-native-vector-icons/MaterialCommunityIcons';
import MI from 'react-native-vector-icons/MaterialIcons';

export default class UHeader extends Component {
  render() {
    return (
      <View style={styles.header}>
        <TouchableOpacity onPress={this.props.onPress}>
          <MCI name="format-align-justify" size={30} color="#ffae00" />
        </TouchableOpacity>
        <Text style={styles.text}>{this.props.title}</Text>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  header: {
    width: '100%',
    height: 75,
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
    paddingLeft: 15,
  },
  text: {
    fontSize: 18,
    paddingLeft: 10,
  },
});
