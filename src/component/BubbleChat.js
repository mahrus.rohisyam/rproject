import React, {Component} from 'react';
import {Text, StyleSheet, View, TouchableOpacity} from 'react-native';
import {CText} from '.';
import {Colors, responsiveHeight, responsiveWidth} from '../assets';

export class BubbleChat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      right: false,
      left: this.props.position,
    };
  }
  componentDidMount() {
    const {right, left} = this.state;
    if (left == false) {
      this.setState({
        right: !right,
      });
    }
  }
  render() {
    const {right, left} = this.state;
    return (
      <View>
        {left ? (
          <View style={styles.wrapperBubble}>
            <TouchableOpacity
              style={styles.bubbleChat}
              onLongPress={this._testLongPress}>
              <CText style={styles.text}>{this.props.content}</CText>
              <CText style={styles.date}>04:20</CText>
            </TouchableOpacity>
          </View>
        ) : (
          <View style={styles.wrapperBubbleRight}>
            <TouchableOpacity
              style={styles.bubbleChatRight}
              onLongPress={this._testLongPress}>
              <CText style={styles.name}>{this.props.name}</CText>
              <CText style={styles.text}>{this.props.content}</CText>
              <CText style={styles.date}>04:20</CText>
            </TouchableOpacity>
          </View>
        )}
      </View>
    );
  }
}
export default BubbleChat;
const styles = StyleSheet.create({
  bubbleChat: {
    minHeight: responsiveHeight(25),
    backgroundColor: Colors.primary,
    borderTopStartRadius: 20,
    borderBottomEndRadius: 20,
  },
  wrapperBubble: {
    paddingLeft: 15,
    paddingVertical: 10,
    minWidth: responsiveWidth(60),
    maxWidth: responsiveWidth(200),
  },
  bubbleChatRight: {
    minHeight: responsiveHeight(25),
    backgroundColor: Colors.secondary,
    borderTopStartRadius: 20,
    borderBottomEndRadius: 20,
  },
  wrapperBubbleRight: {
    paddingVertical: 10,
    paddingRight: 15,
    minWidth: responsiveWidth(60),
    maxWidth: responsiveWidth(420),
    marginLeft: 180,
  },
  date: {
    textAlign: 'right',
    fontSize: 12,
    paddingRight: 15,
    paddingBottom: 10,
    color: Colors.white,
  },
  text: {
    color: Colors.black,
    padding: 15,
    paddingBottom: 5,
  },
});
