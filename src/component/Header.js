import React, {Component} from 'react';
import {Image, Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
export class index extends Component {
  render() {
    return (
      <View style={styles.mainContainer}>
        <View style={styles.blockThree}>
          {/* <Image
            style={styles.logo}
            source={require('../assets/media/LogoMini.png')}
          /> */}
        </View>
        <View style={styles.blockThree}>
          <Text style={styles.title}>Test</Text>
        </View>
        <View style={styles.misc}>
          <TouchableOpacity style={styles.icon} onPress={this.props.onPress}>
            <MaterialIcons name="search" color="white" size={30} />
          </TouchableOpacity>
          <TouchableOpacity style={styles.icon} onPress={this.props.onPress}>
            <MaterialIcons name="notifications" color="white" size={30} />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default index;
const styles = StyleSheet.create({
  mainContainer: {
    height: 70,
    alignItems: 'center',
    backgroundColor: '#1e1e1e',
    flexDirection: 'row',
  },
  title: {
    color: 'white',
    fontSize: 20,
    textAlign: 'center',
  },
  logo: {
    margin: 15,
  },
  misc: {
    alignItems: 'center',
    flexDirection: 'row',
    flex: 1,
  },
  icon: {
    paddingLeft: 15,
  },
  blockThree: {
    flex: 1,
  },
});
