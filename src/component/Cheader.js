import React, {Component} from 'react';
import {Image, Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {CText} from '.';
import {Colors, responsiveHeight} from '../assets';
export class CHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      chat: false,
      general: this.props.chat,
    };
  }
  componentDidMount() {
    const {chat, general} = this.state;
    if (chat == true) {
      this.setState({
        general: !general,
      });
    }
  }
  render() {
    const {general, chat} = this.state;
    return (
      <View style={styles.mainContainer}>
        {general ? (
          <View style={styles.chatWrap}>
            <TouchableOpacity
              style={styles.backButton}
              onPress={this.props.backPress}>
              <MaterialIcons name="arrow-back" color="black" size={30} />
            </TouchableOpacity>

            <CText style={styles.title}>{this.props.title}</CText>

            <View style={styles.iconWrapper}>
              <TouchableOpacity
                style={styles.icon}
                onPress={this.props.onPress}>
                <MaterialIcons name="phone" color="black" size={30} />
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.icon}
                onPress={this.props.onPress}>
                <MaterialIcons name="videocam" color="black" size={30} />
              </TouchableOpacity>
            </View>
          </View>
        ) : (
          <View style={styles.contactWrap}>
            <TouchableOpacity
              style={styles.backPress}
              onPress={this.props.onPress}>
              <MaterialIcons name="arrow-back" color="black" size={30} />
            </TouchableOpacity>
            <CText style={styles.title}>My Contact</CText>
          </View>
        )}
      </View>
    );
  }
}

export default CHeader;
const styles = StyleSheet.create({
  mainContainer: {
    height: responsiveHeight(100),
    alignItems: 'center',
    backgroundColor: 'white',
    flexDirection: 'row',
    elevation: 10,
  },
  title: {
    color: 'black',
    fontSize: 20,
    textAlign: 'center',
  },
  chatWrap: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  iconWrapper: {
    flexDirection: 'row',
    paddingHorizontal: 10,
  },
  icon: {
    paddingHorizontal: 5,
  },
  backButton: {
    marginLeft: 15,
  },
  contactWrap: {
    flexDirection: 'row',
    paddingHorizontal: 10,
  },
});
