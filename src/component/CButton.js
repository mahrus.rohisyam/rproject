import React, {Component} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { Colors, responsiveWidth } from '../assets';
import CText from './CText';
export class CButton extends Component {
  render() {
    const {style} = this.props;
    return (
      <View>
        <TouchableOpacity
          style={{...styles.btn, ...style}}
          onPress={this.props.onPress}>
          <CText style={styles.txt}>{this.props.children}</CText>
          <MaterialIcons
            style={styles.icon}
            name="arrow-forward"
            size={30}
            color="white"
          />
        </TouchableOpacity>
      </View>
    );
  }
}

export default CButton;
const styles = StyleSheet.create({
  btn: {
    backgroundColor: Colors.primary,
    borderRadius: 20,
    width: responsiveWidth(300),
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  txt: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'white',
  },
  icon: {
    position: 'absolute',
    top: 10,
    right: 20,
  },
});
