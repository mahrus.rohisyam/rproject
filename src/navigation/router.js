import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {
  PlayGround,
  reduxExcercise,
  Dzikir,
  Dashboard,
  Login,
  Signup,
  Splash,
  CrudExcercise,
  Mainmenu,
  Fetch,
  Chat,
  Contact,
} from '../Screens';
const Stack = createStackNavigator();
const hide = {headerShown: false};

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Splash">
        <Stack.Screen name="Splash" component={Splash} options={hide} />
        <Stack.Screen name="Login" component={Login} options={hide} />
        <Stack.Screen name="Signup" component={Signup} options={hide} />
        <Stack.Screen name="Dashboard" component={Dashboard} options={hide} />
        <Stack.Screen name="Mainmenu" component={Mainmenu} options={hide} />
        <Stack.Screen name="CrudBasic" component={CrudExcercise} />
        <Stack.Screen name="Redux" component={reduxExcercise} />
        <Stack.Screen name="PlayGround" component={PlayGround} options={hide} />
        <Stack.Screen name="Dzikir" component={Dzikir} options={hide} />
        <Stack.Screen name="Fetch" component={Fetch} options={hide} />
        <Stack.Screen name="Chat" component={Chat} options={hide} />
        <Stack.Screen name="Contact" component={Contact} options={hide} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
export default App;
