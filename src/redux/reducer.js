const initialState = {
  title: 'redux',
  description: 'Learning Management state',
  students: [
    {id: 1, name: 'Russco'},
    {id: 2, name: 'Bambang'},
    {id: 3, name: 'Jemblung'},
  ],
  users: [
    {id: 1, name: 'Russco', password: '123', email: 'mahrusaim@gmail.com'},
    {id: 2, name: 'Bambang', password: '123', email: 'bambang@gmail.com'},
    {id: 3, name: 'Jemblung', password: '123', email: 'jemblung@gmail.com'},
  ],
  currentUser: [],
  isLogin: false,
  currentUser: 'Default',
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'DELETE-DATA':
      return {
        ...state,
        students: state.students.filter(item => item.id != action.payload),
      };
    case 'CHANGE-TITLE':
      return {
        ...state,
        title: action.title,
      };
    case 'ADD-NEW-USER':
      return {
        ...state,
        users: [...state.users, action.payload],
      };
    case 'USER-LOGIN':
      return {
        ...state,
        isLogin: action.payload,
      };
    case 'CURRENT-USER':
      return {
        ...state,
        currentUser: action.payload,
      };
    case 'THEME-SET':
      return {
        ...state,
        theme: action.payload,
      };
    default:
      return state;
  }
};

export default reducer;
