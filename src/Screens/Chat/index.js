import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  ScrollView,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import {BubbleChat, CTextInput, CHeader, CText} from '../../component';
import MI from 'react-native-vector-icons/MaterialIcons';
import MCI from 'react-native-vector-icons/MaterialCommunityIcons';
import {Colors, responsiveHeight, responsiveWidth} from '../../assets';
import {connect} from 'react-redux';
import firestore from '@react-native-firebase/firestore';
export class index extends Component {
  constructor() {
    super();
    this.state = {
      chat: [
        {messageFrom: true, content: 'Hi'},
        {messageFrom: false, content: 'Hi'},
        {messageFrom: false, content: 'Hi'},
        {messageFrom: false, content: 'How Are you Dude'},
      ],
    };
  }
  // componentDidMount(){
  //   firestore()
  //   .collection('Chat')
  //   .doc(this.props.)
  // }
  _submitChat = () => {
    const {message, chat} = this.state;
    const addChat = chat;
    if (message != '') {
      addChat.push({
        content: message,
        messageFrom: false,
      });
      this.setState({
        message: '',
      });
    } else {
      console.log('Isi woy');
    }
  };
  _testLongPress() {
    console.log('Berhasil');
  }
  render() {
    const {message, chat} = this.state;
    const {target} = this.props.route.params;
    return (
      <View style={{flex: 1}}>
        <CHeader
          title={target.name}
          chat={true}
          backPress={() => {
            this.props.navigation.goBack();
          }}
        />

        <ScrollView>
          {chat.map((val, i) => {
            return (
              <BubbleChat
                key={i}
                position={val.messageFrom}
                content={val.content}
              />
            );
          })}
        </ScrollView>

        <View style={styles.chatBox}>
          <View>
            <TextInput
              placeholder="Test"
              style={styles.input}
              onChangeText={typing => {
                this.setState({message: typing});
              }}
              value={message}
            />
          </View>

          <TouchableOpacity>
            <MI name="attach-file" size={30} color="#ffae00" />
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => {
              this._submitChat();
            }}>
            <MCI name="arrow-right-circle" size={30} color="#ffae00" />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    name: state.currentUser,
  };
};
export default connect(mapStateToProps)(index);
const styles = StyleSheet.create({
  chatBox: {
    width: '100%',
    height: responsiveHeight(125),
    backgroundColor: 'white',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.5,
    shadowRadius: 5,

    elevation: 5,
  },
  input: {
    width: responsiveWidth(325),
    height: responsiveHeight(75),
    paddingLeft: 15,
    borderRadius: 30,
    backgroundColor: '#1e1e1e',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  bubbleChat: {
    minHeight: responsiveHeight(25),
    backgroundColor: Colors.primary,
    borderTopStartRadius: 20,
    borderBottomEndRadius: 20,
  },
  wrapperBubble: {
    paddingLeft: 15,
    paddingVertical: 10,
    minWidth: responsiveWidth(60),
    maxWidth: responsiveWidth(200),
  },
  bubbleChatRight: {
    minHeight: responsiveHeight(25),
    backgroundColor: Colors.secondary,
    borderTopStartRadius: 20,
    borderBottomEndRadius: 20,
  },
  wrapperBubbleRight: {
    paddingVertical: 10,
    paddingRight: 15,
    minWidth: responsiveWidth(60),
    maxWidth: responsiveWidth(420),
    marginLeft: 180,
  },
  date: {
    textAlign: 'right',
    fontSize: 12,
    paddingRight: 15,
    paddingBottom: 10,
    color: Colors.white,
  },
  text: {
    color: Colors.black,
    padding: 15,
    paddingBottom: 5,
  },
});
