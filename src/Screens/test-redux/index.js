import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {connect} from 'react-redux';
export class index extends Component {
  render() {
    return (
      <View style={styles.mainContainer}>
        <Text>{this.props.title}</Text>
        <Text>{this.props.description}</Text>
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    title: state.title,
    description: state.description,
  };
};

export default connect(mapStateToProps)(index);

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

