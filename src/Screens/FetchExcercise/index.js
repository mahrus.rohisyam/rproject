import React, { PureComponent } from 'react'
import { Text, View, TouchableOpacity,ScrollView  } from 'react-native'

export default class Fetch extends PureComponent {
    constructor(){
        super()
        this.state={
            data :[
                {id:''},
                {name:''},
                {address:''},
            ]
        }
    }
    componentDidMount=()=>{
        fetch('https://jsonplaceholder.typicode.com/users')
        .then(response => response.json())
        .then(result => this.setState({data:result}))
    }
    
    componentDidUpdate(){
        console.log('Update')
    }

    render() {
        return (
            <ScrollView style={{backgroundColor:'#1e1e1e'}}>
                {this.state.data.map((value, indeks)=>{
                    return(
                     <Text key={indeks}>{
                    `Id = ${value.id} 
                     Name = ${value.name} 
                     Address = ${value.address}
                     `}</Text>
                    )
                })}
                
            </ScrollView>
        )
    }
}
