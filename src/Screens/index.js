import PlayGround from './PlayGround';
import reduxExcercise from './test-redux';
import Dzikir from './Dzikir';
import Dashboard from './DashBoard';
import Login from './Login';
import Signup from './SignUp';
import Splash from './Splash';
import Mainmenu from './MainMenu';
import CrudExcercise from './CrudExcercise';
import Fetch from './FetchExcercise';
import Chat from './Chat';
import Contact from './Contact';
export {
  PlayGround,
  reduxExcercise,
  Dzikir,
  Dashboard,
  Login,
  Signup,
  Splash,
  CrudExcercise,
  Mainmenu,
  Fetch,
  Chat,
  Contact,
};
