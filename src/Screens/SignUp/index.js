import React, {Component} from 'react';
import {Image, Text, View, StyleSheet} from 'react-native';
import {Colors} from '../../assets';
import {connect} from 'react-redux';
import {CButton, CText, CTextInput} from '../../component/';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
export class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newUser: [{username: '', password: '', email: ''}],
      fullname: '',
      email: '',
      password: '',
      viewPassword: '',
      phone: '',
    };
  }
  _register = () => {
    const {email, password, phone, username} = this.state;
    const {navigation} = this.props;

    if (
      email !== undefined &&
      password !== undefined &&
      username !== undefined
    ) {
      auth()
        .createUserWithEmailAndPassword(email, password)
        .then(res => {
          firestore()
            .collection('userData')
            .doc(res.user.uid)
            .set({
              email: email,
              username: username,
              phone: phone,
              uid: res.user.uid,
            })
            .catch(e => {
              console.log(e);
            });
        })
        .catch(error => {
          if (error.code === 'auth/email-already-in-use') {
            console.log('That email address is already in use!');
          }

          if (error.code === 'auth/invalid-email') {
            console.log('That email address is invalid!');
          }

          console.error(error);
        });
      // navigation.goBack();
      this.setState({
        username: '',
        email: '',
        phone: '',
        password: '',
      });
    } else {
      alert('Please fill with valid data');
    }
  };
  render() {
    const {users} = this.props;
    const {username, password, viewPassword, newUser} = this.state;
    return (
      <View style={styles.mainContainer}>
        <CText style={styles.title}>Signup</CText>

        <CText style={styles.text}>Please Register with valid Data</CText>

        <CTextInput
          placeholder="Username"
          style={styles.input}
          onChangeText={typing => this.setState({username: typing})}
        />
        <CTextInput
          placeholder="Email"
          style={styles.input}
          onChangeText={typing => this.setState({email: typing})}
        />
        <CTextInput
          placeholder="Phone Number"
          style={styles.input}
          onChangeText={typing => this.setState({phone: typing})}
        />
        <CTextInput
          placeholder="Password"
          style={styles.input}
          onChangeText={typing => this.setState({password: typing})}
        />

        <View style={styles.button}>
          <CButton
            onPress={() => {
              this._register();
            }}>
            Signup
          </CButton>
        </View>

        <CText style={styles.text}>
          Alreay have account?{' '}
          <CText
            style={styles.link}
            onPress={() => {
              this.props.navigation.goBack();
            }}>
            Click Here !
          </CText>
        </CText>
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    users: state.users,
  };
};
const mapDispatchToProps = send => {
  return {
    addUsers: data =>
      send({
        type: 'ADD-NEW-USER',
        payload: data,
      }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(index);

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#1e1e1e',
  },
  title: {
    color: Colors.primary,
    fontSize: 32,
    paddingLeft: 15,
    fontWeight: 'bold',
  },
  input: {
    marginVertical: 10,
  },
  button: {
    marginTop: 10,
    alignSelf: 'center',
  },
  text: {
    color: Colors.white,
    textAlign: 'center',
  },
  link: {
    color: Colors.secondary,
    textDecorationLine: 'underline',
  },
});
