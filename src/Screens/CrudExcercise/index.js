import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Alert,
} from 'react-native';
import MCI from 'react-native-vector-icons/MaterialCommunityIcons';
export class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        {nama: 'Fulan', alamat: 'Tangerang', id: '1'},
        {nama: 'Fulan', alamat: 'Tangerang', id: '2'},
      ],
      checked: true,
      onEdit: true,
      cancel: true,
      newNama: '',
      newAlamat: '',
      newId: '',
    };
  }
  _addData = () => {
    const {data, nama, alamat, id, newNama, newAlamat, newId} = this.state;
    if (
      newNama == null ||
      (newNama == ' ' && newAlamat == null) ||
      newAlamat == ' '
    ) {
      return Alert.alert(
        'Data tidak Valid !',
        'Harap cek semua data sudah terisi atau anda memasukkan spasi kosong',
      );
    } else if (newNama == null || newNama == ' ' || newNama == '') {
      return Alert.alert(
        'Data tidak Valid !',
        'Harap cek semua data sudah terisi atau anda memasukkan spasi kosong',
      );
    } else if (newAlamat == null || newAlamat == ' ' || newAlamat == '') {
      return Alert.alert(
        'Data tidak Valid !',
        'Harap cek semua data sudah terisi atau anda memasukkan spasi kosong',
      );
    } else {
      const dataBaru = this.state.data;
      dataBaru.push({
        nama: newNama,
        alamat: newAlamat,
        id: newId,
      });
      this.setState({
        newNama: '',
        newAlamat: '',
        newId: '',
      });
    }
  };
  _deleteData = params => {
    const data = this.state.data;
    const newData = data.filter(data => {
      return data != params;
    });
    this.setState({
      data: newData,
    });
  };
  _check = id => {
    this.setState(prevState => ({
      data: prevState.data.map(value => {
        if (value.id == id) {
          value.checked = !value.checked;
          return value;
        } else {
          return value;
        }
      }),
    }));
  };
  _deleteSelectedData = () => {
    Alert.alert(
      'Confirmation !',
      `Are you sure delete selected Data? Note that deleted data couldn't restored.`,
      [
        {
          text: 'Sure !',
          onPress: () => {
            this.setState(prevState => ({
              data: prevState.data.filter(value => !value.checked),
            }));
          },
        },
        {
          text: 'No !',
          onPress: () => {
            console.log('Go back');
          },
        },
      ],
    );
  };

  _onEdit = value => {
    const {cancel, onEdit} = this.state;
    this.setState({
      newNama: value.nama,
      newAlamat: value.alamat,
      newId: value.id,
      onEdit: !onEdit,
      cancel: !cancel,
    });
  };
  _updateData = () => {
    console.log('Berhasil');
    const {newNama, newAlamat, newId, onEdit, id} = this.state;
    this.setState(prevState => ({
      data: prevState.data.map(value => {
        if (value.id == newId) {
          (value.nama = newNama), (value.alamat = newAlamat);
          return value;
        } else {
          return value;
        }
      }),
      onEdit: !onEdit,
      newAlamat: '',
      newNama: '',
      newId: '',
    }));
  };
  _cancel = () => {
    this.setState({
      newId: '',
      newNama: '',
      newAlamat: '',
      onEdit: !this.state.onEdit,
    });
  };
  render() {
    const {data, checked, newAlamat, newNama, onEdit, cancel, newId} =
      this.state;
    return (
      <View>
        <View style={styles.headTable}>
          <Text style={styles.headText}>No</Text>
          <Text style={styles.headText}>Nama</Text>
          <Text style={styles.headText}>Alamat</Text>
          <Text style={styles.headText}>Action</Text>
        </View>
        {data.map((value, index) => {
          return (
            <View style={styles.table} key={index}>
              <Text style={styles.tableText}>{index + 1}</Text>
              <Text style={styles.tableText}>{value.nama}</Text>
              <Text style={styles.tableText}>{value.alamat}</Text>
              <TouchableOpacity
                onPress={() => {
                  this._onEdit(value);
                }}>
                <MCI
                  name="square-edit-outline"
                  size={20}
                  color={value.onEdit ? 'green' : 'black'}
                  style={styles.checkIcon}
                />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  this._check(value.id);
                }}>
                <MCI
                  name={
                    value.checked
                      ? 'checkbox-marked-outline'
                      : 'checkbox-blank-outline'
                  }
                  size={20}
                  color={value.checked ? 'green' : 'black'}
                  style={styles.checkIcon}
                />
              </TouchableOpacity>
            </View>
          );
        })}
        <View style={styles.insertBox}>
          <View style={{paddingBottom: 10}}>
            <TextInput
              style={styles.textInput}
              placeholder="Id"
              onChangeText={typing => this.setState({newId: typing})}
              value={newId}
            />
            <TextInput
              style={styles.textInput}
              placeholder="Name"
              onChangeText={typing => this.setState({newNama: typing})}
              value={newNama}
            />
            <TextInput
              style={styles.textInput}
              placeholder="Alamat"
              onChangeText={typing => this.setState({newAlamat: typing})}
              value={newAlamat}
            />
          </View>
          {onEdit ? (
            <TouchableOpacity
              style={styles.button}
              onPress={() => {
                this._addData();
              }}>
              <Text style={styles.buttonText}>input</Text>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity style={styles.updateButton}>
              <Text
                style={styles.buttonText}
                onPress={() => this._updateData()}>
                Update
              </Text>
            </TouchableOpacity>
          )}
          {onEdit ? (
            <TouchableOpacity
              style={styles.deleteButton}
              onPress={() => {
                this._deleteSelectedData();
              }}>
              <Text style={styles.buttonText}>Delete</Text>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              style={styles.cancelButton}
              onPress={() => {
                this._cancel();
              }}>
              <Text style={{color: 'red'}}>Cancel</Text>
            </TouchableOpacity>
          )}
        </View>
      </View>
    );
  }
}

export default index;
const styles = StyleSheet.create({
  headTable: {
    backgroundColor: 'blue',
    flexDirection: 'row',
    height: '15%',
    alignItems: 'center',
  },
  headText: {
    color: 'white',
    flex: 1,
    textAlign: 'center',
  },
  table: {
    flexDirection: 'row',
    marginTop: 5,
  },
  tableText: {
    width: '25%',
    textAlign: 'center',
  },
  insertBox: {
    backgroundColor: 'black',
    padding: 15,
    margin: 10,
    borderWidth: 1.2,
    borderRadius: 10,
  },
  textInput: {
    backgroundColor: 'darkblue',
    marginTop: 5,
    borderRadius: 10,
    paddingLeft: 15,
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    borderRadius: 10,
    backgroundColor: 'darkblue',
  },
  deleteButton: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    borderRadius: 10,
    backgroundColor: 'red',
  },
  cancelButton: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    borderRadius: 10,
    backgroundColor: 'white',
  },
  updateButton: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    borderRadius: 10,
    backgroundColor: 'green',
  },
  buttonText: {
    color: 'white',
  },
  checkIcon: {
    paddingLeft: 15,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
