import React, {Component} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {connect} from 'react-redux';
import {Logo} from '../../assets/media';
import auth from '@react-native-firebase/auth';
export class index extends Component {
  componentDidMount() {
    auth().onAuthStateChanged(user => {
      setTimeout(() => {
        if (user) {
          this.props.navigation.replace('Dashboard');
        } else {
          this.props.navigation.replace('Login');
        }
      });
    });
  }
  render() {
    return (
      <View style={styles.container}>
        <Image source={Logo} style={styles.image} />
        <Text style={styles.headerText}>R P R O J E C T</Text>
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    isLogin: state.isLogin,
  };
};

export default connect(mapStateToProps)(index);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
    backgroundColor: '#1e1e1e',
  },
  image: {
    alignSelf: 'center',
    width: '43%',
    height: '30%',
  },
  headerText: {
    fontSize: 18,
    color: '#ffae00',
    textAlign: 'center',
    marginTop: 25,
  },
});
