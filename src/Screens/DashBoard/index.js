import React, {Component} from 'react';
import {View, ScrollView, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import {responsiveHeight, responsiveWidth} from '../../assets';
import {Logo} from '../../assets/media';
import {CButton, CText} from '../../component';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
export class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: 'Default Username',
    };
  }
  async componentDidMount() {
    const getData = await auth().currentUser.uid;
    firestore()
      .collection('userData')
      .doc(getData)
      .onSnapshot(res => {
        this.props.user(res.data());
      });
  }
  _logout = () => {
    auth()
      .signOut()
      .then(() => this.props.navigation.replace('Login'));
    this.props.user({});
  };
  render() {
    const {navigate, replace, userName} = this.props.navigation;
    const {user} = this.state;

    return (
      <View>
        <ScrollView>
          <View>
            {/* Background Start */}
            <View style={styles.background}></View>
            <CText style={styles.userName}>
              {this.props.currentUser.username}
            </CText>
            <View style={styles.btn}>
              <CButton
                onPress={() => {
                  navigate('CrudBasic');
                }}>
                CRUD BASIC
              </CButton>

              <CButton
                onPress={() => {
                  navigate('Fetch');
                }}>
                Fetching Excercise
              </CButton>

              <CButton
                onPress={() => {
                  navigate('Dzikir');
                }}>
                Dzikir Counter
              </CButton>

              <CButton
                onPress={() => {
                  navigate('Chat');
                }}>
                Chat Room
              </CButton>

              <CButton
                onPress={() => {
                  navigate('Contact');
                }}
                style={styles.btn}>
                Contact
              </CButton>

              <CButton
                onPress={() => {
                  this._logout();
                }}
                style={styles.logout}>
                Logout
              </CButton>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    isLogin: state.isLogin,
    currentUser: state.currentUser,
  };
};
const mapStateToDispatch = send => {
  return {
    isLogin: data =>
      send({
        type: 'USER-LOGIN',
        payload: data,
      }),
    user: data =>
      send({
        type: 'CURRENT-USER',
        payload: data,
      }),
  };
};
export default connect(mapStateToProps, mapStateToDispatch)(index);
const styles = StyleSheet.create({
  background: {
    height: responsiveHeight(250),
    backgroundColor: '#ffae00',
  },
  userName: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#1e1e1e',
    paddingVertical: 15,
    textAlign: 'center',
  },
  btn: {
    alignItems: 'center',
  },
  logout: {
    backgroundColor: 'red',
  },
});
