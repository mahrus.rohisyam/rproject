import React, {Component} from 'react';
import {View, StyleSheet, Alert} from 'react-native';
import {Colors} from '../../assets';
import {connect} from 'react-redux';
import {CButton, CText, CTextInput} from '../../component/';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/app';
export class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      viewPassword: true,
    };
  }
  // _submit = () => {
  //   const {users, navigation, currentUser, isLogin} = this.props;
  //   const {username, password, user} = this.state;
  //   const checkUserName = users.filter(users => users.name == username);
  //   const currentUserName = checkUserName[0];

  //   if (currentUserName !== undefined) {
  //     if (password == currentUserName.password) {
  //       currentUser(checkUserName);
  //       isLogin(true);
  //       navigation.replace('Dashboard')
  //     } else {
  //       Alert.alert('Data Invalid', `Email or Password did'nt Match`);
  //     }
  //   } else {
  //     Alert.alert('Data Invalid', `Email or Password did'nt Match`);
  //   }
  //   // console.log(currentUser)
  // };

  _submit = () => {
    const {email, password} = this.state;
    if (email !== '' && password !== '') {
      auth()
        .signInWithEmailAndPassword(email, password)
        .then(() => {
          this.props.navigation.replace('Dashboard');
        })
        .catch(error => {
          if (error.code === 'auth/email-already-in-use') {
            console.log('That email address is already in use!');
          }

          if (error.code === 'auth/invalid-email') {
            console.log('That email address is invalid!');
          }
          console.error(error);
        });
    } else {
      alert('Masukin data yang bener');
    }
  };

  _register = () => {
    this.props.navigation.navigate('Signup');
  };

  render() {
    const {password, username} = this.state;

    return (
      <View style={styles.mainContainer}>
        <CText style={styles.title}>Login</CText>

        <CText style={styles.text}>
          You don’t think you should login first and behave like human not
          robot.
        </CText>

        <CTextInput
          placeholder="Username"
          style={styles.input}
          onChangeText={typing => this.setState({email: typing})}
        />

        <CTextInput
          onChangeText={typing => this.setState({password: typing})}
          placeholder="Password"
          style={styles.input}
        />

        <View style={styles.button}>
          <CButton
            onPress={() => {
              this._submit();
            }}>
            Login
          </CButton>
        </View>

        <CText style={styles.text}>
          Forgot Password?{' '}
          <CText
            style={styles.link}
            onPress={() => {
              this._forgot();
            }}>
            Click Here !
          </CText>
        </CText>

        <CText style={styles.text}>
          New User?{' '}
          <CText
            style={styles.link}
            onPress={() => {
              this._register();
            }}>
            Register
          </CText>
        </CText>
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    users: state.users,
    isLogin: state.isLogin,
  };
};
const mapStateToDispatch = send => {
  return {
    isLogin: data =>
      send({
        type: 'USER-LOGIN',
        payload: data,
      }),
    currentUser: data =>
      send({
        type: 'CURRENT-USER',
        payload: data,
      }),
  };
};

export default connect(mapStateToProps, mapStateToDispatch)(index);

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#1e1e1e',
  },
  title: {
    color: Colors.primary,
    fontSize: 32,
    paddingLeft: 15,
    fontWeight: 'bold',
  },
  input: {
    marginVertical: 10,
  },
  button: {
    marginTop: 10,
    alignSelf: 'center',
  },
  text: {
    color: Colors.white,
    textAlign: 'center',
  },
  link: {
    color: Colors.secondary,
    textDecorationLine: 'underline',
  },
});
