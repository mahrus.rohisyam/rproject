import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  TextInput,
} from 'react-native';
import { Dzikir } from '../../assets/media';
export default class index extends Component {
  constructor() {
    super();
    this.state = {
      dzikir: 0,
    };
  }
  tambah() {
    if (this.state.dzikir == 33) {
      this.setState({
        dzikir: 0,
      });
    } else {
      this.setState({
        dzikir: this.state.dzikir + 1,
      });
    }
  }
  reset() {
    this.setState({
      dzikir: 0,
    });
  }
  render() {
    return (
      <View style={styles.container}>
        <Image
          style={{width: '40%', height: '20%', alignSelf: 'center'}}
          source={Dzikir}
        />
        <Text style={{color: '#55ff99', fontSize: 36}}>Tobat Yuk!</Text>
        <Text style={{color: '#fff', fontSize: 36}}>{this.state.dzikir}</Text>
        <TouchableOpacity
          style={styles.Dzikir}
          onPress={() => {
            this.tambah();
          }}>
          <Text style={{color: '#fff', fontSize: 25}}>Dzikir</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{padding: 10, alignItems: 'center'}}
          onPress={() => {
            this.reset();
          }}>
          <Text style={{color: '#fff', fontSize: 15}}>Mulai Ulang</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1a1a1a',
    alignItems: 'center',
    justifyContent: 'center',
  },
  Dzikir: {
    borderWidth: 1.3,
    borderColor: '#55ff99',
    borderRadius: 150,
    padding: 10,
    alignItems: 'center',
    width: 150,
    height: 150,
    marginTop: 20,
    justifyContent: 'center',
  },
});
