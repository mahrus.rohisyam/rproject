import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import {CHeader, CText} from '../../component';
import {connect} from 'react-redux';
import {Colors, responsiveHeight, responsiveWidth} from '../../assets';
export class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      contact: [],
    };
  }
  async componentDidMount() {
    firestore()
      .collection('userData')
      .where('uid', '!=', this.props.currentUser.uid)
      .onSnapshot(val => {
        this.setState({
          contact: val.docs.map(res => {
            return res.data();
          }),
        });
      });
  }
  _chatRoom = () => {
    const {contact} = this.state;
    this.props.navigation.navigate('Chat', {target: contact.username});
  };
  render() {
    const {contact} = this.state;
    const {navigation, currentUser} = this.props;

    return (
      <View style={styles.mainContainer}>
        <CHeader
          searchBar={false}
          onPress={() => {
            navigation.goBack();
          }}
        />
        <ScrollView>
          {contact.map((val, indeks) => {
            return (
              <TouchableOpacity
                key={indeks}
                onPress={() => {
                  navigation.navigate('Chat', {
                    target: val,
                  });
                }}>
                <View style={styles.card}>
                  <View style={styles.pp} />
                  <View>
                    <CText style={styles.text}>{val.username}</CText>
                    <CText style={styles.text}>{val.phone}</CText>
                  </View>
                </View>
              </TouchableOpacity>
            );
          })}
        </ScrollView>
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    currentUser: state.currentUser,
  };
};
export default connect(mapStateToProps)(index);
const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: Colors.black,
  },
  card: {
    flexDirection: 'row',
    marginVertical: 10,
    marginHorizontal: 10,
    paddingLeft: 15,
    paddingVertical: 10,
    backgroundColor: Colors.secondary,
    height: responsiveHeight(125),
    borderRadius: 20,
  },
  pp: {
    width: responsiveWidth(80),
    height: responsiveHeight(100),
    backgroundColor: Colors.primary,
    borderRadius: 20,
  },
  text: {
    paddingLeft: 10,
    paddingTop: 10,
    color: Colors.black,
  },
});
