const Colors = {
  primary: '#f4a261',
  secondary: '#2a9d8f',
  red: '#e63946',
  yellow: '#ffd166',
  black: '#1e1e1e',
  white: 'white',
};
const themes = {
  light: {
    primary: '#f4a261',
    secondary: '#2a9d8f',
    red: '#e63946',
    yellow: '#ffd166',
    black: 'white',
    white: '#1e1e1e',
  },
  dark: {
    primary: '#f4a261',
    secondary: '#2a9d8f',
    red: '#e63946',
    yellow: '#ffd166',
    black: '#1e1e1e',
    white: 'white',
  },
};
export {Colors};
